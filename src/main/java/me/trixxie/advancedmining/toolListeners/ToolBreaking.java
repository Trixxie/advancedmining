package me.trixxie.advancedmining.toolListeners;

import me.trixxie.advancedmining.AdvancedMining;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;

public class ToolBreaking implements Listener {

    AdvancedMining plugin;

    public ToolBreaking(AdvancedMining plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void toolBreaking(BlockBreakEvent e){
        Player p = e.getPlayer();
        ItemStack tool = p.getInventory().getItemInMainHand();
        Damageable meta = (Damageable) tool.getItemMeta();
        if(meta != null) {
            if (!p.hasPermission("advancedmining.bypass.toolbreak")) {
                if ((tool.getType().getMaxDurability() - meta.getDamage()) == 1) {
                    e.setCancelled(true);
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.toolBreaking")));
                    p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_BREAK, 1, 1);
                }
            }
        }
    }
}
