package me.trixxie.advancedmining.commands;

import me.trixxie.advancedmining.AdvancedMining;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class mainCommand implements CommandExecutor {

    AdvancedMining plugin;

    public mainCommand(AdvancedMining plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(command.getName().equalsIgnoreCase("advancedmining")){
            if(args.length == 0){
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8>&e>&6> &6&lAdvanced&e&lMining &6<&e<&8<"));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "Version &e" + plugin.getDescription().getVersion() + "&7, Developed by &e_Trixxie &7"));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/am reload &7Reloads plugin configuration file"));
            }
            if(args.length == 1 && args[0].equalsIgnoreCase("reload")){
                if(sender.hasPermission("advancedmining.admin")){
                    plugin.reloadConfig();
                    plugin.checkChance();
                    plugin.checkConfig();
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aAdvancedMining was successfully reloaded."));
                } else {
                    sender.sendMessage(ChatColor.RED + "&cYou don't have permission to do this.");
                }
            }
        }
        return false;
    }
}
