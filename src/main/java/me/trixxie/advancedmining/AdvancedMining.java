package me.trixxie.advancedmining;

import me.trixxie.advancedmining.commands.mainCommand;
import me.trixxie.advancedmining.listeners.BlockLayers;
import me.trixxie.advancedmining.listeners.InfestedBlocks;
import me.trixxie.advancedmining.listeners.OreMining;
import me.trixxie.advancedmining.toolListeners.ToolBreaking;
import me.trixxie.advancedmining.utils.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Consumer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Logger;

public final class AdvancedMining extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        Logger logger = this.getLogger();

        getConfig().options().copyDefaults();
        saveDefaultConfig();
        checkChance();
        checkConfig();

        // Bstats.org metrics
        int pluginId = 10165; // <-- Replace with the id of your plugin!
        Metrics metrics = new Metrics(this, pluginId);

        // Registering events
        getServer().getPluginManager().registerEvents(new OreMining(this), this);
        getServer().getPluginManager().registerEvents(new InfestedBlocks(this), this);
        getServer().getPluginManager().registerEvents(new BlockLayers(this), this);
        getServer().getPluginManager().registerEvents(new ToolBreaking(this), this);

        // Registering commands
        this.getCommand("advancedmining").setExecutor(new mainCommand(this));

        // Spigot resource update checker
        new UpdateChecker(this, 88299).getVersion(version -> {
            if (this.getDescription().getVersion().equalsIgnoreCase(version)) {
                logger.info("There is not a new update available.");
            } else {
                logger.info("There is a new update available.");
                logger.info("Download it now: https://www.spigotmc.org/resources/advancedmining.88299/");
            }
        });

        logger.info("Everything is fine and ready to go!");
    }



    public void checkConfig() {
        FileConfiguration config = this.getConfig();
        Logger log = this.getLogger();

        if(!config.contains("messages.toolBreaking")){
            config.addDefault("messages.toolBreaking", "\"&cYour tool is too brittle to break any blocks.\"");
            saveConfig();
            log.warning("Your config was missing: \"messages.toolBreaking\"");
        }
    }

    public void checkChance(){
        if(this.getConfig().getBoolean("drop-double.enabled")){
            if(this.getConfig().getInt("drop-double.chance") > 100){
                this.getConfig().set("drop-double.chance", 100);
                this.saveConfig();
                this.getLogger().warning("There was a error in config.yml, chance was set above 100. Chance has been set to 100.");
            }
            if(this.getConfig().getInt("drop-double.chance") <= 0){
                this.getConfig().set("drop-double.chance", 0);
                this.saveConfig();
                this.getLogger().warning("There was a error in config.yml, chance was set to 0 or below it. Chance has been set to 1.");
            }
        }
    }

    public class UpdateChecker {

        private JavaPlugin plugin;
        private int resourceId;

        public UpdateChecker(JavaPlugin plugin, int resourceId) {
            this.plugin = plugin;
            this.resourceId = resourceId;
        }

        public void getVersion(final Consumer<String> consumer) {
            Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> {
                try (InputStream inputStream = new URL("https://api.spigotmc.org/legacy/update.php?resource=" + this.resourceId).openStream(); Scanner scanner = new Scanner(inputStream)) {
                    if (scanner.hasNext()) {
                        consumer.accept(scanner.next());
                    }
                } catch (IOException exception) {
                    this.plugin.getLogger().info("Cannot look for updates: " + exception.getMessage());
                }
            });
        }
    }
}
