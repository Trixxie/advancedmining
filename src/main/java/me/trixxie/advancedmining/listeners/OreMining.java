package me.trixxie.advancedmining.listeners;

import me.trixxie.advancedmining.AdvancedMining;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class OreMining implements Listener {

    AdvancedMining plugin;

    public OreMining(AdvancedMining plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onOreMining(BlockBreakEvent e){
        Player p = e.getPlayer();
        Block b = e.getBlock();
        Location l = b.getLocation();
        if(!b.getDrops(p.getInventory().getItemInMainHand()).isEmpty()){
            dropDouble(l,b,p);
            if(plugin.getConfig().getBoolean("ignore-silkTouch")){
                if(p.getInventory().getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH)) {
                    if (b.getType().equals(Material.COAL_ORE)) {
                        e.setDropItems(false);
                        l.getWorld().dropItemNaturally(l, new ItemStack(Material.COAL));
                    } else if (b.getType().equals(Material.EMERALD_ORE)) {
                        e.setDropItems(false);
                        l.getWorld().dropItemNaturally(l, new ItemStack(Material.EMERALD));
                    } else if (b.getType().equals(Material.DIAMOND_ORE)) {
                        e.setDropItems(false);
                        l.getWorld().dropItemNaturally(l, new ItemStack(Material.DIAMOND));
                    } else if (b.getType().equals(Material.REDSTONE_ORE)) {
                        e.setDropItems(false);
                        for (int i = 0; i < 4; i++) {
                            l.getWorld().dropItemNaturally(l, new ItemStack(Material.REDSTONE));
                        }
                    } else if (b.getType().equals(Material.NETHER_QUARTZ_ORE)) {
                        e.setDropItems(false);
                        l.getWorld().dropItemNaturally(l, new ItemStack(Material.QUARTZ));
                    } else if (b.getType().equals(Material.LAPIS_ORE)) {
                        e.setDropItems(false);
                        for (int i = 0; i < 4; i++) {
                            l.getWorld().dropItemNaturally(l, new ItemStack(Material.LAPIS_LAZULI));
                        }
                    } else if (b.getType().equals(Material.NETHER_GOLD_ORE)) {
                        e.setDropItems(false);
                        l.getWorld().dropItemNaturally(l, new ItemStack(Material.GOLD_INGOT));
                    }
                }
            }
            if(plugin.getConfig().getBoolean("auto-smelt")){
                if(plugin.getConfig().getBoolean("ignore-silkTouch")) {
                    if (plugin.getConfig().getBoolean("autoSmelt-usePermission")) {
                        if (p.hasPermission("advancedmining.smelt")) {
                            if (b.getType().equals(Material.IRON_ORE)) {
                                e.setDropItems(false);
                                l.getWorld().dropItemNaturally(l, new ItemStack(Material.IRON_INGOT));
                            } else if (b.getType().equals(Material.GOLD_ORE)) {
                                e.setDropItems(false);
                                l.getWorld().dropItemNaturally(l, new ItemStack(Material.GOLD_INGOT));
                            } else if (b.getType().equals(Material.ANCIENT_DEBRIS)) {
                                e.setDropItems(false);
                                l.getWorld().dropItemNaturally(l, new ItemStack(Material.NETHERITE_SCRAP));
                            }
                        }
                    } else {
                        if (b.getType().equals(Material.IRON_ORE)) {
                            e.setDropItems(false);
                            l.getWorld().dropItemNaturally(l, new ItemStack(Material.IRON_INGOT));
                        } else if (b.getType().equals(Material.GOLD_ORE)) {
                            e.setDropItems(false);
                            l.getWorld().dropItemNaturally(l, new ItemStack(Material.GOLD_INGOT));
                        } else if (b.getType().equals(Material.ANCIENT_DEBRIS)) {
                            e.setDropItems(false);
                            l.getWorld().dropItemNaturally(l, new ItemStack(Material.NETHERITE_SCRAP));
                        }
                    }
                }
            }
        }
    }

    private void dropDouble(Location location, Block block, Player p) {
        Material bt = block.getType();
        String luckyMsg = plugin.getConfig().getString("messages.youGotLucky");
        if (plugin.getConfig().getBoolean("auto-smelt")) {
            if (plugin.getConfig().getBoolean("ignore-silkTouch")) {
                if (bt.equals(Material.IRON_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("IRON_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.IRON_INGOT));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.GOLD_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("GOLD_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.GOLD_INGOT));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.ANCIENT_DEBRIS)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("ANCIENT_DEBRIS")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.NETHERITE_SCRAP));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.DIAMOND_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("DIAMOND_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.DIAMOND));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.REDSTONE_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("REDSTONE_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.REDSTONE));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.EMERALD_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("EMERALD_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.EMERALD));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.COAL_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("COAL_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.COAL));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.LAPIS_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("LAPIS_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.LAPIS_LAZULI));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.NETHER_GOLD_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("NETHER_GOLD_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.GOLD_INGOT));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                } else if (bt.equals(Material.NETHER_QUARTZ_ORE)) {
                    if (plugin.getConfig().getBoolean("drop-double.enabled") && !plugin.getConfig().getList("drop-double.ignoreBlocks").contains("NETHER_QUARTZ_ORE")) {
                        if(getChance(plugin.getConfig().getInt("drop-double.chance"))) {
                            location.getWorld().dropItemNaturally(location, new ItemStack(Material.QUARTZ));
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', luckyMsg));
                        }
                    }
                }
            }
        }
    }

    private boolean getChance(int min) {
        Random random = new Random();
        return random.nextInt(99) + 1 <= min;
    }
}
