package me.trixxie.advancedmining.listeners;

import me.trixxie.advancedmining.AdvancedMining;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class BlockLayers implements Listener {

    AdvancedMining plugin;

    public BlockLayers(AdvancedMining plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void stoneLayers(BlockBreakEvent e){
        Block b = e.getBlock();
        Player p = e.getPlayer();
        if(b.getType().equals(Material.STONE)) {
            if (plugin.getConfig().getBoolean("layersEnabled")) {
                if (!p.getGameMode().equals(GameMode.CREATIVE)) {
                    if(!e.isCancelled()) {
                        if (!b.getDrops(p.getInventory().getItemInMainHand()).isEmpty()) {
                            if (p.getInventory().getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH)) {
                                if (plugin.getConfig().getBoolean("ignore-silkTouch")) {
                                    e.setCancelled(true);
                                    b.getLocation().getWorld().dropItemNaturally(b.getLocation(), new ItemStack(Material.COBBLESTONE));
                                    b.setType(Material.COBBLESTONE);
                                }
                            } else {
                                e.setCancelled(true);
                                b.getLocation().getWorld().dropItemNaturally(b.getLocation(), new ItemStack(Material.COBBLESTONE));
                                b.setType(Material.COBBLESTONE);
                            }
                        }
                    }
                }
            }
        }
    }
}
