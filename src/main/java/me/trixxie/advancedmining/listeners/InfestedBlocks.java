package me.trixxie.advancedmining.listeners;

import me.trixxie.advancedmining.AdvancedMining;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.HashSet;
import java.util.Random;

public class InfestedBlocks implements Listener {

    AdvancedMining plugin;

    public InfestedBlocks(AdvancedMining plugin) {
        this.plugin = plugin;
    }

    public static HashSet<Material> infestedBlocks = new HashSet<>();

    static{
        infestedBlocks.add(Material.COAL_ORE);
        infestedBlocks.add(Material.GOLD_ORE);
        infestedBlocks.add(Material.IRON_ORE);
        infestedBlocks.add(Material.DIAMOND_ORE);
        infestedBlocks.add(Material.EMERALD_ORE);
        infestedBlocks.add(Material.REDSTONE_ORE);
        infestedBlocks.add(Material.LAPIS_ORE);
    }

    @EventHandler
    public void InfestedOres(BlockBreakEvent e){
        Player p = e.getPlayer();
        Material bt = e.getBlock().getType();
        Location bl = e.getBlock().getLocation();
        if(plugin.getConfig().getBoolean("infestedOres")){
            if(!p.getGameMode().equals(GameMode.CREATIVE)) {
                if (infestedBlocks.contains(bt)) {
                    if (getChance(90)) {
                        bl.getWorld().spawnEntity(bl, EntityType.SILVERFISH);
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.blockWasInfested")));
                    }
                }
            }
        }
    }

    private boolean getChance(int min) {
        Random random = new Random();
        return random.nextInt(99) + 1 >= min;
    }
}
